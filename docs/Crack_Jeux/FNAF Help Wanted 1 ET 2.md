# FNAF Help Wanted 1 et 2

Lien de téléchargement du 1 [https://www.clictune.com/jN6s](https://www.clictune.com/jN6s)

Lien de téléchargement du 2 : [https://www.clictune.com/jN6r](https://www.clictune.com/jN6r)

!!! warning "Torrent"
    
    Ce téléchargement requiert l'utilisation d'un logiciel de téléchargement compatible avec le protocole [BitTorrent](https://fr.wikipedia.org/wiki/BitTorrent) tel que [qBitTorrent](https://www.qbittorrent.org/), [uTorrent](https://www.utorrent.com/) ou Transmission pour les vrais (:eyes:), accompagné d'un VPN. L'auteur n'est responsable d'aucun problème lié a l'utilisation de ces logiciels ~~et d'un éventuel courrier de l'ARCOM~~. Des bisous.

