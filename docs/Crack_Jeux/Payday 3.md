# Payday 3


Lien de téléchagement : [https://www.clictune.com/jN6e](https://www.clictune.com/jN6e)


!!! warning "Torrent"
    
    Ce téléchargement requiert l'utilisation d'un logiciel de téléchargement compatible avec le protocole [BitTorrent](https://fr.wikipedia.org/wiki/BitTorrent) tel que [qBitTorrent](https://www.qbittorrent.org/), [uTorrent](https://www.utorrent.com/) ou Transmission pour les vrais (:eyes:), accompagné d'un VPN. L'auteur n'est responsable d'aucun problème lié a l'utilisation de ces logiciels ~~et d'un éventuel courrier de l'ARCOM~~. Des bisous.

NOTE : Avant de télécharger, vous devez savoir :
Le cracks sont pris sur online-fix.me. Comme ce jeu est principalement en ligne, même en mode solo. Par conséquent, le crack peut ne pas fonctionner à l'avenir si le développeur le corrige côté serveur. Ce jeu n'est pas trop volumineux, vous pouvez donc considérer cela comme une version d'essai si vous avez une bonne connexion internet pour télécharger 😉

========================
COMMENT LANCER LE JEU :

Lancez le client Steam.
Un compte Microsoft Store est requis pour fonctionner, vous devrez donc en créer un.
Vous devez créer un compte Nebula (https://nebula.starbreeze.com) et le lier à votre compte Microsoft Store. (sinon, vous recevrez une fenêtre de connexion/mot de passe dans le jeu).
Démarrez le jeu depuis "XboxLiveAuth.exe".
Connectez-vous à votre compte Microsoft Store. Dans le jeu : "Connexion" : Acceptez une invitation d'un ami "Création d'un serveur".
========================
REMARQUES :

Vous pouvez jouer sur des serveurs officiels.
Pour que les invitations fonctionnent, vous devez avoir activé l'option de crossplay, sinon les invitations ne seront pas envoyées.
Si, lorsque vous lancez le jeu, une fenêtre apparaît où vous devez entrer le nom d'utilisateur et le mot de passe de votre compte Nebula, cela signifie que vous n'avez pas lié votre compte Microsoft Store à votre compte Nebula.
Les DLC ne sont pas actuellement déverrouillés. (à explorer plus tard)
Certains logiciels dont vous pourriez avoir besoin (si votre PC ne les a pas installés) :
https://dotnet.microsoft.com/fr-fr/download/dotnet/thank-you/runtime-desktop-5.0.17-windows-x86-installer

Crédit à online-fix.me.