# Satisfactory

Partie 1 : [https://mega.nz/file/bENWSC4R#SLbLapJMM3eYo3ej2MhYIc9hn5l6XKf9funYbgo26pM](https://mega.nz/file/bENWSC4R#SLbLapJMM3eYo3ej2MhYIc9hn5l6XKf9funYbgo26pM)

Clé de déchiffrement (si nécesaire) : `SLbLapJMM3eYo3ej2MhYIc9hn5l6XKf9funYbgo26pM`

Partie 2 : [https://mega.nz/file/aUEByIYA#FoxjCOAy5A2tAtrGcJTyB-oJW6Nfk0I1cv7cE8K_Vvo](https://mega.nz/file/aUEByIYA#FoxjCOAy5A2tAtrGcJTyB-oJW6Nfk0I1cv7cE8K_Vvo)

Clé de déchiffrement (si nécessaire) : `FoxjCOAy5A2tAtrGcJTyB-oJW6Nfk0I1cv7cE8K_Vvo`

