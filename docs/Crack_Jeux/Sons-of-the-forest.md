# Sons of the Forest

![](https://cdn.discordapp.com/attachments/1078706695074754580/1078706866399486062/414097_63bd639bd0a25.jpg)

Lien : [https://www.clictune.com/jMTp](https://www.clictune.com/jMTp)

Mot de passe : `online-fix.me`

Il faut bien remplacer les fichiers du jeu par les fichier présent dans le dossier "fix" ! 

!!! warning "Torrent"
    
    Ce téléchargement requiert l'utilisation d'un logiciel de téléchargement compatible avec le protocole [BitTorrent](https://fr.wikipedia.org/wiki/BitTorrent) tel que [qBitTorrent](https://www.qbittorrent.org/), [uTorrent](https://www.utorrent.com/) ou Transmission pour les vrais (:eyes:), accompagné d'un VPN. L'auteur n'est responsable d'aucun problème lié a l'utilisation de ces logiciels ~~et d'un éventuel courrier de l'ARCOM~~. Des bisous.