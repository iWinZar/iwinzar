# Politique de conformité au DMCA

Nous accordons une attention particulière à toutes les réclamations d'infractions aux droits d'auteur et prenons des mesures appropriées dans les délais les plus brefs. Si un logiciel ou un jeu protégé par des droits d'auteur se trouve sur notre site et que vous souhaitez qu'il soit retiré, veuillez nous fournir une notification écrite contenant les informations suivantes :

    Adresse physique
    Numéro de téléphone
    Adresse e-mail
    Site web

Veuillez envoyer cette notification d'infraction à [j.l.emeraud@gmail.com](mailto:j.l.emeraud@gmail.com). Je traite chaque notification avec diligence et vous assure une réponse dans les 72 heures. Si vous ne recevez pas de réponse dans ce délai, veuillez actualiser la page correspondante et vous constaterez que le contenu a été retiré.

Il est important de noter que ce site ne cautionne pas le piratage et que les liens proposés sont destinés exclusivement à des fins de sauvegarde. Nous vous encourageons à ne télécharger aucun fichier si vous n'êtes pas en possession du support original. Nous vous encourageons également à soutenir les développeurs de jeux et de logiciels en achetant leurs produits si vous les appréciez.

<hr>

We pay particular attention to all claims of copyright infringement and take appropriate action as soon as possible. If any copyrighted software or game is on our site and you would like it removed, please provide us with written notification containing the following information:

    Physical address
    Telephone number
    e-mail address
    Website address

Please send this infringement notification to [j.l.emeraud@gmail.com](mailto:j.l.emeraud@gmail.com). I process each notification with diligence and assures you of a response within 72 hours. If you do not receive a response within this time, please refresh the corresponding page and you will see that the content has been removed.

It is important to note that this site does not endorse piracy and that the links provided are for backup purposes only. We encourage you not to download any files unless you are in possession of the original media. We also encourage you to support game and software developers by purchasing their products if you like them.