# Serveur privé Genshin Impact

## Tutoriel vidéo

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/eMkXGncWNDs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Tutoriel écrit

### Prérequis

- Windows 10 ou supérieur
- Antivirus désactivé

### Téléchargements

- [.NET 6.0 Runtime (Desktop)](https://download.visualstudio.microsoft.com/download/pr/30841ca9-5538-40c3-9022-d1ba1e69f6e8/aa94715bc3d74ee0b2e27de757ef0cdb/windowsdesktop-runtime-6.0.19-win-x64.exe)
- [YuukiPS Launcher](https://github.com/YuukiPS/Launcher-PC/releases/download/2023.4.19.1809/YuukiPS.zip)

### Installation

Télécharger Le .NET Runtime dans les liens au dessus puis ouvrir le fichier pour l'installation.

Extraire le fichier `YuukiPS.zip` dans un dossier

### Connexion au serveur privé

Pour commencer, créez un compte sur le site officiel de [YuukiPS](https://ps.yuuki.me/account/login)

Ensuite, ouvrez le fichier `YuukiPS.exe`. Une fenêtre noire va d'abbord s'ouvrir, puis la fênètre principale va s'ouvrir

![](https://github.com/YuukiPS/Launcher-PC/raw/master/Docs/show.jpg)

Double-cliquez sur YuukiPS dans la liste à droite, puis launch. Le launcher va télécharger un fichier puis lancer le jeu. La encore, une fenêtre noire va s'ouvrir. Cela indique que vous êtes bien connectés au serveur privé. Le jeu va ensuite se lancer normalement. A la fenêtre de connection, le nom d'utilisateur est celui que vous avez choisi a l'inscription. Pour le mot de passe, ce n'est pas celui de l'inscription. La fonctionnalité n'est pas encore implémentée donc mettre n'importe quel mot de passe (oui, oui, même un seul espace) fonctionnera.

## Liste des commandes et des ID des persos

Téléchargement [ici](../static/GM%20Handbook%20-%20FR.txt)